var { object, string } = require('yup')

module.exports = {
    registerSchema: object().shape({
        name: string().required(),
        password: string().required()
            .min(8)
            .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
        phone: string().required(),
        email: string().required().email(),
    }),
    LoginSchema: object().shape({
        username: string().required(),
        password: string().required()
            .min(8)
            .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
    }),
    ResetPasswordSchema: object().shape({
        password: string().required()
            .min(8)
            .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
    })
} 