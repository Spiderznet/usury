// Update with your config settings.
require('dotenv').config();


module.exports = {

  dev:{
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'tfttrade_ttf',
      password: 'Ve!3I-ef~Z5u',
      database: 'tfttrade_ttf'
    },
    migrations: {
      tableName: 'usury_migrations'
    }
  },

  development: {
    client: 'mysql',
    connection: {
      host: process.env.host || '127.0.0.1',
      user: process.env.user || 'root',
      password: process.env.password || '',
      database: process.env.database || 'test'
    },
    migrations: {
      tableName: 'usury_migrations'
    }
  },

  development1: {
    client: 'sqlite3',
    connection: {
      filename: "./engine/mydb.sqlite"
    },
    migrations: {
      tableName: 'usury_migrations'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      host: process.env.host || '127.0.0.1',
      user: process.env.user || 'your_database_user',
      password: process.env.password || 'your_database_password',
      database: process.env.database || 'myapp_test'
    },
    migrations: {
      tableName: 'usury_migrations'
    }
  }

};
