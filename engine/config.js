const ROLES = {
    root: 1,
    admin: 2,
    collector: 3,
    loaner: 4
}

module.exports = {
    ROLES
}