
var logger = require('morgan');
var fs = require('fs')
var path = require('path')
var moment = require('moment')


// create a write stream (in append mode)
let dir = path.join(__dirname, '../public/logs')
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true })
}

var accessLogStream = fs.createWriteStream(`${dir}/${moment().format('DD-MM-YYYY')}-access.log`, { flags: 'a' })

module.exports = logger(function (tokens, req, res) {

    let log = [
        moment().format(),
        req.user_id || 'guest',
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        tokens['response-time'](req, res) + ' ms',
        req.headers['user-agent'],
    ].join('|')

    if (req.body) {
        log += '|' + JSON.stringify(req.body)
    }
    return log
}, { stream: accessLogStream })
