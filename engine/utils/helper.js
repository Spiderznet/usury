var fs = require('fs');
const Payments = require('../models/Payments');

let unlinkFile = (path) => {
    if (fs.existsSync('public' + path)) {
        fs.unlink('public' + path, function (err) {
            console.log('File deleted on public' + path)
        });
    }
    return true;
}

const makePayment = async (user_id, amount, description = "") => {

    let lastTransation = await Payments.query()
        .findOne({ user_id })
        .orderBy('id', 'DESC')
        .select('balance');

    let balance = 0
    if (lastTransation && lastTransation.balance) {
        balance = lastTransation.balance;
    }
    if (balance <= 0) {
        throw new Error('Insufficient balance')
    }
    balance = balance + Number(amount)
    if (balance < 0) {
        throw new Error('Insufficient balance')
    }

    let result = await Payments.query()
        .insert({
            user_id,
            amount,
            balance,
            description
        });

    return result
}


module.exports = {
    unlinkFile,
    makePayment
}