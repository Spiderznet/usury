const upload = (query, { _where = null, _sort = null, _limit = null, _offset = null } = {}) => {
    
    
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            // Uploads is the Upload_folder_name 
            callback(null, "public/uploads/profile")
        },
        filename: function (req, file, callback) {
            callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
        }
    });
    const maxSize = 1 * 1000 * 1000;
    
    var upload = multer({
        storage: storage,
        limits: { fileSize: maxSize },
        fileFilter: function (req, file, cb) {
    
            // Set the filetypes, it is optional 
            var filetypes = /jpeg|jpg|png/;
            var mimetype = filetypes.test(file.mimetype);
    
            var extname = filetypes.test(path.extname(
                file.originalname).toLowerCase());
    
            if (mimetype && extname) {
                return cb(null, true);
            }
    
            cb("Error: File upload only supports the "
                + "following filetypes - " + filetypes);
        }
    
        // mypic is the name of file attribute 
    }).single("image");

    return query
}

module.exports = upload