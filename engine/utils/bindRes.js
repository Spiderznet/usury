let bindRes = (error = false, msg, res, data = []) => {
    let status = 0;
    if (error) {
        status = 0;
        msg = msg || error;
        res.json({ status, msg, data });
    } else {
        status = 1;
        res.json({ status, msg, data });
    }
}

module.exports = bindRes;