const parseQuery = (query, { _where = null, _sort = null, _limit = null, _offset = null } = {}) => {
    if (_where) {
        Object.keys(_where).forEach(item => {
            let key = item.split('_');
            let method = key.pop();
            let field = key.join('_');

            if (method == "" || method == 'eq') query.where(field, '=', _where[item])
            if (method == 'ne') query.where(field, 'not', _where[item])
            if (method == 'lt') query.where(field, '<', _where[item])
            if (method == 'lte') query.where(field, '<=', _where[item])
            if (method == 'gt') query.where(field, '>', _where[item])
            if (method == 'gte') query.where(field, '>=', _where[item])
            if (method == 'in') query.whereIn(field, _where[item])
            if (method == 'nin') query.whereNotIn(field, _where[item])
            if (method == 'contains') query.where(field, 'like', `%${_where[item]}%`)
        })
    }
    if (_sort) {
        Object.keys(_sort).forEach(field => {
            let value = _sort[field] || 'ASC'
            value.toUpperCase();
            if (['ASC', 'DESC'].includes(value)) {
                query.orderBy(field, value)
            }
        })
    }
    if (_limit) {
        query.limit(_limit)
    }
    if (_offset) {
        query.offset(_offset)
    }
    return query
}

module.exports = parseQuery