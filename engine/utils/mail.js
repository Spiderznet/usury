const nodemailer = require("nodemailer");
const fs = require('fs');
let ejs = require('ejs')
require('dotenv').config({ path: '../.env' });
var path = require('path');
// var templateDir = path.join(process.cwd(), '/views/');


let transport = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
    }
});



const render = (templatename, props) => {

    return new Promise((resolve, reject) => {
        defaultPops = {
            appName: 'TFT',
            baseURL: 'http://localhost:8100'
        }
        ejs.renderFile(path.resolve(`./views/${templatename}.ejs`), { ...props, ...defaultPops }, (err, data) => {
            if (err) return reject(err)

            resolve(data)
        })

    })

}

const sendMail = (props) => {
    return new Promise((resolve, reject) => {
        const message = {
            from: process.env.MAIL_FROM,
            to: process.env.MAIL_TO,
            subject: '',
            html: '',
            ...props
        };
        transport.sendMail(message, function (error, info) {
            if (error) {
                reject(error)
            } else {
                resolve(info)
            }
        })

    })
}

// (async () => {
//     var result = await await render('mail/welcome',{
//         appName:'TFT',
//         baseURL:'http://localhost:8100'
//     })
//     var mailresponse = await sendMail({
//         to:'viky.viky884@gmail.com',
//         html: result
//     })
//     console.log(mailresponse)
// })()
module.exports = { sendMail, render }