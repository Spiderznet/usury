
exports.up = function (knex) {
    return knex.schema.createTable('usury_payments', function (table) {
        table.increments();
        table.integer('user_id')
        // table.foreign('user_id').references('usury_users.id')
        table.integer('amount').defaultTo(0)
        table.integer('balance').defaultTo(0)
        table.string('description').defaultTo(null)

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('usury_payments')
};
