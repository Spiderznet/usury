
exports.up = function (knex) {
    return knex.schema.createTable('usury_expenses', function (table) {
        table.increments();
        table.integer('payments_id')
        // table.foreign('payment_id').references('usury_payments.id')
        table.integer('user_id')
        // table.foreign('user_id').references('usury_users.id')
        table.integer('amount').defaultTo(0)
        table.string('description').defaultTo(null)

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('usury_expenses')
};
