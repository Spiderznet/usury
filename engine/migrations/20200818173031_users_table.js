
exports.up = function (knex) {
    return knex.schema.createTable('usury_users', function (table) {
        table.increments()
        table.string('name').notNullable()
        table.string('email').notNullable().unique();
        table.string('password').notNullable();
        table.integer('role_id').defaultTo(0);
        // table.foreign('role_id').references('usury_roles.role_id')
        table.string('phone').notNullable().unique();
       
        table.text('image').nullable();
        table.integer('is_confirm').defaultTo(0);
        table.integer('is_email_confirm').defaultTo(0);
        table.integer('is_active').defaultTo(0);
       
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    // }).raw('ALTER TABLE usury_users AUTO_INCREMENT = 10000')
    })

  

    .createTable('usury_setting', function (table) {
        table.increments()
        table.string('name').notNullable()
        table.string('value').defaultTo(null);
    })

    .createTable('usury_roles', function (table) {
        table.increments('role_id')
        table.string('name').notNullable()
        table.string('description').defaultTo(null);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('usury_users')
 
    .dropTable('usury_setting')
    .dropTable('usury_roles');
    
};
