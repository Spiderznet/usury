
exports.up = function (knex) {
    return knex.schema.createTable('usury_loans', function (table) {
        table.increments();
        table.integer('payment_id')
        // table.foreign('payment_id').references('usury_payments.id')
        table.integer('user_id')
        // table.foreign('user_id').references('usury_users.id')
        table.integer('collector_id')
        // table.foreign('collector_id').references('usury_users.id')
        table.integer('principal_amount').defaultTo(0)
        table.integer('intrest').defaultTo(0)
        table.integer('intrest_amount').defaultTo(0)
        table.string('interest_type').defaultTo(null)
        table.integer('total_amount').defaultTo(0)
        table.integer('duration').defaultTo(0)
        table.string('duration_type').defaultTo(null)
        table.integer('instalment_cycle').defaultTo(0)
        table.string('instalment_cycle_type').defaultTo(null)
        table.integer('no_of_instalment').defaultTo(0)
        table.integer('instalment_amount').defaultTo(0)
        table.enu('status', ['IN_PROGRESS', 'COMPLETED']).defaultTo('IN_PROGRESS')
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('usury_loans')
};
