
exports.up = function (knex) {
    return knex.schema.createTable('usury_instalments', function (table) {
        table.increments();
        table.integer('payment_id')
        // table.foreign('payment_id').references('usury_payments.id')
        table.integer('loan_id')
        // table.foreign('loan_id').references('usury_loans.id')
        table.integer('amount').defaultTo(0)

        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('usury_instalments')
};
