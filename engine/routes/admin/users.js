var express = require('express');
var router = express.Router();
var db = require('../../utils/db')
var { verifyJWTToken, getInfo, createJWToken } = require('../../utils/jwt');
var { SHA256: hash } = require('crypto-js');
var { registerSchema } = require('../../schema/users');
var multer = require('multer');
const bindRes = require('../../utils/bindRes');
const path = require('path')
var fs = require('fs');
var { sendMail, render } = require('../../utils/mail');
const Users = require('../../models/users');
const Roles = require('../../models/roles');
const parseQuery = require('../../utils/ParseQuery');

const accountSid = 'AC05dc98b19ae2758d50e3d21e44ada8aa';
const authToken = '6ccd841182cb9d289ede3467f22a3f7e';
const client = require('twilio')(accountSid, authToken);

// New user register
// params - name, phone, email, password


var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        callback(null, "public/uploads/profile")
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
    }
});
const maxSize = 1 * 1000 * 1000;

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
    fileFilter: function (req, file, cb) {

        // Set the filetypes, it is optional 
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);

        var extname = filetypes.test(path.extname(
            file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }

        cb("Error: File upload only supports the "
            + "following filetypes - " + filetypes);
    }

    // mypic is the name of file attribute 
}).single("image");


router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {
        let roles = await Roles.query().select();
        bindRes(null, 'Add user details', res, { roles })
    } catch (err) {
        bindRes(true, "Request failed", res)
    }
})

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let { search = "", ...query } = req.query
        let usersQuery = Users.query();
        usersQuery = parseQuery(usersQuery, query)

        if (search) {
            usersQuery.andWhere(function () {

                this.where('id', 'like', `%${search}%`)
                    .orWhere('name', 'like', `%${search}%`)
                    .orWhere('email', 'like', `%${search}%`)
                    .orWhere('phone', 'like', `%${search}%`)
            })
        }

        let user = await usersQuery.select();
        bindRes(null, 'User List', res, user)
    } catch (err) {
        bindRes(true, "Request failed", res, err.toString())
    }
})
router.post('/', verifyJWTToken, upload, async (req, res) => {
    try {
        let { name, email, password, role_id, phone, is_active } = req.body
        let { file } = req

        let data = {
            name,
            email,
            password: hash(password).toString(),
            role_id: role_id.id,
            phone,
            is_confirm: 1,
            is_email_confirm: 1,
            is_active: is_active ? 1 : 0,
        }

        if (file) {
            data = { ...data, image: '/uploads/profile/' + file.filename }
        }

        let result = await Users.query().insert(data)
        bindRes(null, 'User Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "User Create failed", res, err.toString())
    }
})

router.get('/:user_id', verifyJWTToken, async (req, res, next) => {
    try {
        let { user_id } = req.params;
        let user = await Users.query()
            .select()
            .findOne({ "id": user_id })
            .withGraphFetched('role');

        if (user) {
            delete user.password;
            bindRes(null, 'Profile Details', res, user);
        } else {
            bindRes(null, 'Invalid User', res);
        }

    } catch (err) {
        bindRes(true, err, res, err.toString())
    }
});

router.put('/:user_id', verifyJWTToken, upload, async (req, res) => {
    try {
        let { name, email, password, role_id, phone, is_active } = req.body
        let { file } = req
        let { user_id } = req.params;

        let userDetails = await Users.query().select('id').where({ email }).orWhere({ phone }).first()
        if (userDetails && userDetails.id != user_id) {
            bindRes(true, "Phone or Email already exist.", res)
            return
        }

        let data = {
            name,
            email,
            role_id: role_id.id,
            phone,
            is_confirm: 1,
            is_email_confirm: 1,
            is_active: is_active ? 1 : 0,
        }
        if (password) {
            data = { ...data, password: hash(password).toString() }
        }

        if (file) {
            data = { ...data, image: '/uploads/profile/' + file.filename }
        }

        let result = await Users.query().update(data).where({ id: user_id })
        bindRes(null, 'User List', res, { id: user_id })
    } catch (err) {
        bindRes(true, "Request failed", res, err.toString())
    }
})

router.delete('/:user_id', verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req.params
        let result = await Users.query().delete().where({ id: user_id })
        bindRes(null, "Deleted Success!", res, result)
    } catch (err) {
        bindRes(true, "Request failed", res, err.toString())
    }
})

module.exports = router; 
