var express = require('express');
const Loans = require('../../models/Loans');
const Payments = require('../../models/Payments');
const bindRes = require('../../utils/bindRes');
const { makePayment } = require('../../utils/helper');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let { search = "", ...query } = req.query
        let loanQuery = Loans.query();
        loanQuery = parseQuery(loanQuery, query)

        if (search) {
            loanQuery.andWhere(function () {

                this.where('id', 'like', `%${search}%`)
                    .orWhere('user_id', 'like', `%${search}%`)
            })
        }

        let result = await loanQuery.select()
            .withGraphFetched('user')
            .withGraphFetched('instalments')
            .withGraphFetched('collector');

        bindRes(null, 'Loan list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Loan list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Loans.query()
            .findOne({ id })
            .select()
            .withGraphFetched('user')
            .withGraphFetched('instalments')
            .withGraphFetched('collector');

        bindRes(null, 'Loan details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Loan details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { user_id, principal_amount, intrest, intrest_amount, interest_type, total_amount, duration, duration_type, instalment_cycle, instalment_cycle_type, no_of_instalment, instalment_amount } = req.body
        let { user_id: collector_id } = req
        let { payment_id } = await makePayment(collector_id, - (principal_amount), `New Loan from ${collector_id} to ${user_id.id}`)
        // let { payment_id } = await makePayment(user_id.id, Math.abs(principal_amount), `New Loan from ${collector_id} to ${user_id.id}`)

        let result = await Loans.query()
            .insert({
                payment_id,
                user_id: user_id.id,
                collector_id,
                principal_amount,
                intrest,
                intrest_amount,
                interest_type,
                total_amount,
                duration,
                duration_type,
                instalment_cycle,
                instalment_cycle_type,
                no_of_instalment,
                instalment_amount,
                status: 'IN_PROGRESS'
            });

        bindRes(null, 'Loan Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Loan Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { user_id, amount, balance, description } = req.body
        let result = await Loans.query()
            .findOne({ id })
            .update({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Loan update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Loan update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Loans.query().findOne({ id }).delete();

        bindRes(null, 'Loan deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Loan deleted failed", res, err.toString())
    }
})

module.exports = router