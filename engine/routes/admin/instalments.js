var express = require('express');
const { ROLES } = require('../../config');
const Instalments = require('../../models/Instalments');
const Loans = require('../../models/Loans');
const bindRes = require('../../utils/bindRes');
const { makePayment } = require('../../utils/helper');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let { search = "", ...query } = req.query
        let { role_id, user_id } = req
        let instalmentQuery = Instalments.query();
        instalmentQuery = parseQuery(instalmentQuery, query)

        if (role_id == ROLES.collector) {
            instalmentQuery.join('usury_loans', 'usury_loans.id', '=', 'usury_instalments.loan_id').where('usury_loans.collector_id', '=', user_id)
        }

        let result = await instalmentQuery.select();

        bindRes(null, 'Instalment list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Instalment list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Instalments.query().findOne({ id }).select();

        bindRes(null, 'Instalment details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Instalment details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { loan_id } = req.body
        let loanDetails = await Loans.query()
            .findById(loan_id)
            .withGraphFetched('instalments')
        let { id: payment_id } = await makePayment(
            loanDetails.collector_id,
            loanDetails.instalment_amount,
            `${loanDetails.instalments.length + 1}/${loanDetails.no_of_instalment} instalment of ${loan_id} loan`
        )
        if (loanDetails) {
            let result = await Instalments.query()
                .insert({
                    payment_id,
                    loan_id,
                    amount: loanDetails.instalment_amount,
                });
            bindRes(null, 'Instalment Create successfuly', res, result)
        }
        else {
            bindRes(true, "Lone not found", res)
        }

    } catch (err) {
        bindRes(true, "Instalment Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { user_id, amount, balance, description } = req.body
        let result = await Instalments.query()
            .findOne({ id })
            .update({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Instalment update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Instalment update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Instalments.query().findOne({ id }).delete();

        bindRes(null, 'Instalment deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Instalment deleted failed", res, err.toString())
    }
})

module.exports = router