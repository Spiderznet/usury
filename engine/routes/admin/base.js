var express = require('express');
const Payments = require('../../models/Payments');
const bindRes = require('../../utils/bindRes');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let paymentQuery = Payments.query();
        paymentQuery = parseQuery(paymentQuery, req.query)

        let result = await paymentQuery.select();

        bindRes(null, 'Payment list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Payment list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Payments.query().findOne({ id }).select();

        bindRes(null, 'Payment details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Payment details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { user_id, amount, balance, description } = req.body
        let result = await Payments.query()
            .insert({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Payment Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Payment Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { user_id, amount, balance, description } = req.body
        let result = await Payments.query()
            .findOne({ id })
            .update({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Payment update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Payment update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Payments.query().findOne({ id }).delete();

        bindRes(null, 'Payment deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Payment deleted failed", res, err.toString())
    }
})

module.exports = router