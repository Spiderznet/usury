var express = require('express');
var router = express.Router();
const usersRouter = require('./users')
const paymentsRouter = require('./payments')
const loansRouter = require('./loans')
const instalmentsRouter = require('./instalments')
const expensessRouter = require('./expenses')


router.use('/users', usersRouter)
router.use('/payments', paymentsRouter)
router.use('/loans', loansRouter)
router.use('/instalments', instalmentsRouter)
router.use('/expenses', expensessRouter)


module.exports = router