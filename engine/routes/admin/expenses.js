var express = require('express');
const Expenses = require('../../models/Expenses');
const bindRes = require('../../utils/bindRes');
const { makePayment } = require('../../utils/helper');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let expensesQuery = Expenses.query();
        expensesQuery = parseQuery(expensesQuery, req.query)

        let result = await expensesQuery.select()
            .withGraphFetched('user');

        bindRes(null, 'Expenses list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Expenses list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Expenses.query()
            .findOne({ id })
            .select()
            .withGraphFetched('user');

        bindRes(null, 'Expenses details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Expenses details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { amount, description } = req.body
        let { user_id } = req
        amount = -(amount)
        let { id: payments_id } = await makePayment(user_id, amount, `Expenses of ${user_id}`)
        let result = await Expenses.query()
            .insert({
                payments_id,
                user_id,
                amount,
                description,
            });

        bindRes(null, 'Expenses Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Expenses Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { user_id, amount, balance, description } = req.body
        let result = await Expenses.query()
            .findOne({ id })
            .update({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Expenses update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Expenses update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Expenses.query().findOne({ id }).delete();

        bindRes(null, 'Expenses deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Expenses deleted failed", res, err.toString())
    }
})

module.exports = router