var express = require('express');
const { render } = require('../utils/mail');
const { verifyJWTToken } = require('../utils/jwt');

var router = express.Router();
const moment = require('moment');

var userTable = require('../migrations/20200818173031_users_table')
var db = require('../utils/db')


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express' });
});

// router.get("/dashboard", verifyJWTToken, async (req, res) => {
//     try {
//         let { user_id } = req
//         let month = { tasksPosted: 0, tasksDone: 0 }
//         month.tasksPosted = await Tasks.query()
//             .count('task_id as tasksPosted')
//             .where({ created_by: user_id })
//             .andWhere('created_at', '>', moment().subtract(1, 'months').format('YYYY-MM-D H:mm:ss'))
//             .first() || { tasksPosted: 0 }
//         month.tasksPosted = month.tasksPosted.tasksPosted
//         month.tasksDone = await Bids.query()
//             .count('bid_id as tasksDone')
//             .where({ bid_by: user_id, status: 2 })
//             .andWhere('bid_at', '>', moment().subtract(1, 'months').format('YYYY-MM-D H:mm:ss'))
//             .first() || { tasksDone: 0 }
//         month.tasksDone = month.tasksDone.tasksDone

//         let overall = { tasksPosted: 0, tasksDone: 0 }
//         overall.tasksPosted = await Tasks.query()
//             .count('task_id as tasksPosted')
//             .where({ created_by: user_id })
//             .first() || { tasksPosted: 0 }
//         overall.tasksPosted = overall.tasksPosted.tasksPosted
//         overall.tasksDone = await Bids.query()
//             .count('bid_id as tasksDone')
//             .where({ bid_by: user_id, status: 2 })
//             .first() || { tasksDone: 0 }
//         overall.tasksDone = overall.tasksDone.tasksDone

//         let { current_balance: wallet = 0 } = await Wallets.query()
//             .select('current_balance')
//             .findOne({ 'user_id': req.user_id })
//             .orderBy('wallet_id', 'DESC') || {};

//         bindRes(null, "Dashboard Details", res, { month, overall, wallet })
//     } catch (err) {
//         bindRes(true, "Dashboard Request faild", res, err.toString())
//     }
// })

// router.get('/test/:id', async (req, res) => {
//     let { id } = req.params
//     req.app.io
//         .of(`/socket/notification/${id}`)
//         .emit('update')

//     // newNamespace
//     res.send('ok')
//     // let result = await render('mail/welcome',{
//     //     appName:'TFT',
//     //     baseURL:'http://localhost:8100'
//     // })
//     // res.send(result)

// })

router.get('/migrate',function(req,res,nxt){

    userTable.up(db)

    res.json({
        mssage:'migrate'
    })

})

module.exports = router;
