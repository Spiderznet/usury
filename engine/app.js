require('dotenv').config()
var express = require('express');
var path = require('path');
var fs = require('fs');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var bodyParser = require('body-parser');
// var Sentry = require("@sentry/node");
// var Tracing = require("@sentry/tracing");
var serverless = require('serverless-http');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var adminRouter = require('./routes/admin');
// const logger = require('./utils/logger');
const { getUserIfExist } = require('./utils/jwt');

function loadDB() {
    const DBDir = './mydb.sqlite';
    const tmpDir = require('os').tmpdir();

    if(!fs.existsSync(tmpDir+'/mydb.sqlite')) {
        fs.copyFileSync(DBDir,tmpDir+'/mydb.sqlite')
    }
}

// loadDB();

var app = express();

// Sentry.init({
//     dsn: "https://6b02e20d00494850a0332616f78adc6e@o384713.ingest.sentry.io/5570307",
//     integrations: [
//         // enable HTTP calls tracing
//         new Sentry.Integrations.Http({ tracing: true }),
//         // enable Express.js middleware tracing
//         new Tracing.Integrations.Express({ app }),
//     ],

//     // We recommend adjusting this value in production, or using tracesSampler
//     // for finer control
//     tracesSampleRate: 1.0,
// });

// app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
// app.use(Sentry.Handlers.tracingHandler());
app.use(cors())
app.use(bodyParser.json());
app.use(getUserIfExist);
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(logger);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/admin', adminRouter)
app.use('/users', usersRouter);
app.use('/auth', authRouter);

// The error handler must be before any other error middleware and after all controllers
// app.use(Sentry.Handlers.errorHandler());

// module.exports = app;

// const handler = serverless(app);

module.exports = app