
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('usury_roles').del()
    .then(function () {
      // Inserts seed entries
      return knex('usury_roles').insert([
        { name: 'root', description: "Root user" },
        { name: 'admin', description: "Admin user" },
        { name: 'collector', description: "Loan Collector" },
        { name: 'loaner', description: "Loaner" },

      ]);
    });
};
