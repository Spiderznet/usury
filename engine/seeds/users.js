
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('usury_users').del()
    .then(function () {
      // Inserts seed entries
      return knex('usury_users').insert([
        {
          "name": "Nixon Guerrero",
          "email": "nixonguerrero@medmex.com",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f", // Password@123
          "phone":"9876543210",
          "role_id":"1"
        },
        {
          "name": "Grace Larson",
          "email": "gracelarson@medmex.com",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "phone":"9874561233",
          "role_id":"3"
        },
        {
          "name": "Laurel Bolton",
          "email": "laurelbolton@medmex.com",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "phone":"98745618237",
          "role_id":"3"
        },
        {
          "name": "Sasha Nichols",
          "email": "sashanichols@medmex.com",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "phone":"9874561287",
          "role_id":"4"
        },
        {
          "name": "Clare Estrada",
          "email": "clareestrada@medmex.com",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "phone":"9674561237",
          "role_id":"4"
        },
        {
          "name": "Mathews Gonzalez",
          "email": "mathewsgonzalez@medmex.com",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "phone":"9874561237",
          "role_id":"4"
        }
      ]);
    });
};
