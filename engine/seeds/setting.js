
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('usury_setting').del()
    .then(function () {
      // Inserts seed entries
      return knex('usury_setting').insert([
        { name: 'siteName', value: "Usury Interest" },
        { name: 'logo', value: "/assets/images/TFT.svg" },
        { name: 'upload_max_filesize', value: "5M" }
      ]);
    });
};
