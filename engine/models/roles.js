const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Roles extends Model {
    static get tableName() {
        return 'usury_roles'
    }

    static get relationMappings() {
        
        var Users = require('../models/users');
        return {
            user: {
                relation: Model.HasManyRelation,
                modelClass: Users,
                join: {
                    from: 'usury_roles.role_id',
                    to: 'usury_users.role_id'
                }
            }
        }
    }
}

module.exports = Roles;
