const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Expenses extends Model {
    static get tableName() {
        return 'usury_expenses'
    }

    static get relationMappings() {
        
        const Payments = require('./Payments');
        const Users = require('./users');
        return {
            payment: {
                relation: Model.HasOneRelation,
                modelClass: Payments,
                join: {
                    from: 'usury_expenses.payment_id',
                    to: 'usury_payments.id'
                }
            },
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                join: {
                    from: 'usury_expenses.user_id',
                    to: 'usury_users.id'
                }
            },
        }
    }
}

module.exports = Expenses;