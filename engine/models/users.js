const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Users extends Model {
    static get tableName() {
        return 'usury_users'
    }

    static get idColumn() {
        return 'id';
    }

    static get relationMappings() {
        var Roles = require('../models/roles');

        return {
            role: {
                relation: Model.BelongsToOneRelation,
                modelClass: Roles,
                join: {
                    from: 'usury_users.role_id',
                    to: 'usury_roles.role_id'
                }
            },
        }
    }
}

module.exports = Users;
