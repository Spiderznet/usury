const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Settings extends Model {
    static get tableName() {
        return 'usury_setting'
    }
    
    static get relationMappings() {

    }
}

module.exports = Settings;
