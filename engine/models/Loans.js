const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');



// Give the knex instance to objection.
Model.knex(db);

class Loans extends Model {
    static get tableName() {
        return 'usury_loans'
    }

    static get relationMappings() {
        
        const Users = require('./users');
        const Payments = require('./Payments');
        const Instalments = require('./Instalments');

        return {
            payment: {
                relation: Model.HasOneRelation,
                modelClass: Payments,
                join: {
                    from: 'usury_loans.payment_id',
                    to: 'usury_payments.id'
                }
            },
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                join: {
                    from: 'usury_loans.user_id',
                    to: 'usury_users.id'
                }
            },
            collector: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                join: {
                    from: 'usury_loans.collector_id',
                    to: 'usury_users.id'
                }
            },
            instalments:{
                relation: Model.HasManyRelation,
                modelClass: Instalments,
                join: {
                    from: 'usury_loans.id',
                    to: 'usury_instalments.loan_id'
                }
            }
        }
    }
}

module.exports = Loans;