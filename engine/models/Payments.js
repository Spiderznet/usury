const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Payments extends Model {
    static get tableName() {
        return 'usury_payments'
    }

    static get relationMappings() {

        var Users = require('./users');
        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                filter: query => query.select('name', 'id'),
                join: {
                    from: 'usury_payments.user_id',
                    to: 'usury_users.id'
                }
            },
        }
    }
}

module.exports = Payments;