const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Instalments extends Model {
    static get tableName() {
        return 'usury_instalments'
    }

    static get relationMappings() {
        
        const Loans = require('./Loans');
        const Payments = require('./Payments');
        return {
            payment: {
                relation: Model.HasOneRelation,
                modelClass: Payments,
                join: {
                    from: 'usury_instalments.payment_id',
                    to: 'usury_payments.id'
                }
            },
            loan: {
                relation: Model.HasOneRelation,
                modelClass: Loans,
                join: {
                    from: 'usury_instalments.loan_id',
                    to: 'usury_loans.id'
                }
            },
        }
    }
}

module.exports = Instalments;