import { adminRoot } from "./defaultValues";
import { UserRole } from "../helpers/authHelper"

const data = [
    {
        id: "dashboards",
        icon: "iconsminds-shop-4",
        label: "Dashboard",
        to: "/app/dashboard",
        roles: [UserRole.Admin, UserRole.root, UserRole.collector, UserRole.loaner]
    },
    {
        id: 'user',
        icon: 'simple-icon-people',
        label: 'User',
        to: `${adminRoot}/users`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create User',
                to: `${adminRoot}/users/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List User',
                to: `${adminRoot}/users`,
            },
        ],
    },
    {
        id: 'loans',
        icon: 'iconsminds-financial',
        label: 'Loan',
        to: `${adminRoot}/loans`,
        roles: [UserRole.Admin, UserRole.root, UserRole.collector],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create Loan',
                to: `${adminRoot}/loans/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List Loan',
                to: `${adminRoot}/loans`,
            },
        ],
    },
    {
        id: 'transactions',
        icon: 'iconsminds-handshake',
        label: 'Transactions',
        to: `${adminRoot}/transactions`,
        roles: [UserRole.Admin, UserRole.root, UserRole.collector],
    },
    {
        id: 'instalments',
        icon: 'simple-icon-briefcase ',
        label: 'Instalments',
        to: `${adminRoot}/instalments`,
        roles: [UserRole.Admin, UserRole.root, UserRole.collector],
    },
    {
        id: 'expenses',
        icon: 'simple-icon-cup',
        label: 'Expenses',
        to: `${adminRoot}/expenses`,
        roles: [UserRole.Admin, UserRole.root, UserRole.collector],
    },
];
export default data;
