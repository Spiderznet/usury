const data = [
  {
    title: 'Total Courses',
    icon: 'simple-icon-book-open',
    value: 14
  },
  {
    title: 'Total Participants',
    icon: 'simple-icon-people',
    value: 32,
  },
  {
    title: 'Active Courses',
    icon: 'simple-icon-graduation',
    value: 74,
  },
  {
    title: 'Total Facilitator',
    icon: 'simple-icon-user',
    value: 25
  },
];
export default data;
