import React, { useEffect, useState } from 'react'
import moment from 'moment'
import { stringify } from 'qs';
import { Card, CardBody, CardTitle, Col, Row } from 'reactstrap';
import { getCurrentUser } from '../../helpers/Utils';
import DatePicker from '../../components/Datepicker';
import { LineChart } from '../../components/charts';
import { ThemeColors } from '../../helpers/ThemeColors';
import { UserRole } from '../../helpers/authHelper';
import { PaymentsServices } from '../../utils/APIServices';
import { formatAmount } from '../../utils';

const colors = ThemeColors();


const TransactionChart = ({ ...props }) => {
    let [lineChartData, setLineChartData] = useState(null)
    let [data, setData] = useState([])
    let [isLoading, setIsloading] = useState(false)
    let userDetails = getCurrentUser()
    const [startDate, setStartDate] = useState(moment().subtract(1, 'month').toDate());
    const [endDate, setEndDate] = useState(moment().toDate());
    let [outFlow, setOutFlow] = useState(0)
    let [inFlow, setInFlow] = useState(0)
    let [balance, setBalance] = useState(0)

    const displayChartData = () => {
        setLineChartData(null)
        let groupData = data.reduce((final, { amount, created_at, description }) => {
            // if (description == "ADMIN") return final
            console.log({ startDate })
            if (!(startDate < new Date(created_at) && moment(endDate).endOf('day').toDate() >= new Date(created_at))) return final;
            final[moment(created_at).format('DD/MM/YYYY')] = final[moment(created_at).format('DD/MM/YYYY')] || {
                outFlow: 0,
                inFlow: 0,
            }
            if (amount > 0) {
                final[moment(created_at).format('DD/MM/YYYY')]['inFlow'] += amount
            }
            if (amount < 0) {
                final[moment(created_at).format('DD/MM/YYYY')]['outFlow'] += amount
            }
            return final
        }, {})
        let chartDataLocal = {
            labels: Object.keys(groupData),
            datasets: [
                {
                    label: 'Out Flow',
                    data: [],
                    borderColor: colors.danger,
                    pointBackgroundColor: colors.foregroundColor,
                    pointBorderColor: colors.danger,
                    pointHoverBackgroundColor: colors.danger,
                    pointHoverBorderColor: colors.foregroundColor,
                    pointRadius: 6,
                    pointBorderWidth: 2,
                    pointHoverRadius: 8,
                    fill: false,
                },
                {
                    label: 'In Flow',
                    data: [],
                    borderColor: colors.green,
                    pointBackgroundColor: colors.foregroundColor,
                    pointBorderColor: colors.green,
                    pointHoverBackgroundColor: colors.green,
                    pointHoverBorderColor: colors.foregroundColor,
                    pointRadius: 6,
                    pointBorderWidth: 2,
                    pointHoverRadius: 8,
                    fill: false,
                }
            ]
        }
        Object.keys(groupData).forEach(key => {
            chartDataLocal.datasets[0]['data'].push(Math.abs(groupData[key].outFlow))
            chartDataLocal.datasets[1]['data'].push(groupData[key].inFlow)
        })
        setLineChartData(chartDataLocal)
        setOutFlow(chartDataLocal.datasets[0]['data'].reduce((t, i) => t += i, 0))
        setInFlow(chartDataLocal.datasets[1]['data'].reduce((t, i) => t += i, 0))
    }

    const getList = () => {
        setIsloading(true)
        let query = ''
        let _where = {}
        if (![UserRole.root, UserRole.Admin].includes(userDetails.role)) {
            _where = {
                ..._where,
                user_id_eq: userDetails.user_id
            }
        }
        query = "?" + stringify({
            _where,
            _sort: {
                created_at: "DESC"
            }
        }, { encode: false })
        PaymentsServices.list(query).then(({ status, data }) => {
            if (status) {
                let [first = {}] = data
                setBalance(first.balance)
                setData(data.reverse());

                setIsloading(false)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    useEffect(() => {
        displayChartData()
    }, [startDate, endDate, data])

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <Row>
            <Col xs={12} className="d-md-flex">
                <Card className="mb-2 flex-row mr-3">
                    <Card className="bg-success text-white align-items-center justify-content-center">
                        <div className="px-4">
                            <i className="simple-icon-arrow-down-circle h1 m-0" />
                        </div>
                    </Card>
                    <CardBody className="py-3">
                        <div>
                            <div className="h4 m-0">In Flow</div>
                            <div className="h3 m-0 text-success">{formatAmount(inFlow)}</div>
                        </div>
                    </CardBody>
                </Card>
                <Card className="mb-2 flex-row mr-3">
                    <Card className="bg-danger text-white align-items-center justify-content-center">
                        <div className="px-4">
                            <i className="simple-icon-arrow-up-circle h1 m-0" />
                        </div>
                    </Card>
                    <CardBody className="py-3">
                        <div>
                            <div className="h4 m-0">Out Flow</div>
                            <div className="h3 m-0 text-danger">{formatAmount(outFlow)}</div>
                        </div>
                    </CardBody>
                </Card>
                <Card className="mb-2 flex-row mr-3">
                    <Card className="bg-primary text-white align-items-center justify-content-center">
                        <div className="px-4">
                            <i className="simple-icon-wallet h1 m-0" />
                        </div>
                    </Card>
                    <CardBody className="py-3">
                        <div>
                            <div className="h4 m-0">Balance</div>
                            <div className="h3 m-0 text-primary">{formatAmount(inFlow - outFlow)}</div>
                        </div>
                    </CardBody>
                </Card>
            </Col>
            <Col xs={12}>
                <Card body>
                    <CardTitle>Transactions</CardTitle>
                    <div className="d-flex align-items-center mb-2">
                        <DatePicker
                            selected={startDate}
                            onChange={(name, date) => setStartDate(date)}
                            selectsStart
                            startDate={startDate}
                            endDate={endDate}
                            dateFormat="dd MMM yyyy"
                        />
                        <div className="px-2">To</div>
                        <DatePicker
                            selected={endDate}
                            onChange={(name, date) => setEndDate(date)}
                            selectsEnd
                            startDate={startDate}
                            endDate={endDate}
                            minDate={startDate}
                            dateFormat="dd MMM yyyy"
                        />
                    </div>
                    <div>
                        {
                            lineChartData !== null && (
                                <LineChart data={lineChartData} />
                            )
                        }
                    </div>
                </Card>
            </Col>

        </Row>
    )
}

export default TransactionChart