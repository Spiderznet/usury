import React from 'react'
import Select from 'react-select/async'

const SingleSelectAsync = ({ invalid = false, className = '', name, onChange, onBlur, ...props }) => {
    const handleChange = (val) => {
        onChange(name, val);
    };

    const handleBlur = () => {
        onBlur(name, true);
    };

    return (
        <Select
            className={`react-select ${invalid ? 'is-invalid' : ''} ${className} ${name ? 'field-' + name : ''}`}
            classNamePrefix="react-select"
            getOptionLabel={option => option.name}
            getOptionValue={option => option.id}
            onChange={handleChange}
            onBlur={handleBlur}
            {...props}
        />
    )
}

export default SingleSelectAsync