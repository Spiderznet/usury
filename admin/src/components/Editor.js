import React from 'react'
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; // ES6


const Editor = ({ invalid = false, className = '', name = '', onChange = () => { }, onBlur = () => { }, ...props }) => {
    const handleChange = (val) => {
        onChange(name, val);
    };

    const handleBlur = () => {
        onBlur(name, true);
    };

    return (
        <ReactQuill
            className={`react-quill ${invalid ? 'is-invalid' : ''} ${className} ${name ? 'field-' + name : ''}`}
            onChange={handleChange}
            onBlur={handleBlur}
            {...props}
        />
    )
}

export default Editor