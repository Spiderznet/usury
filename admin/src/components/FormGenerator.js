import React from 'react'
import { Row, Col, FormGroup, Label, FormFeedback, Form, Input, Button, CustomInput } from 'reactstrap'
import useFocusError from './useFocusError'
import DatePicker from './Datepicker'
import Editor from './Editor'
import Switch from 'rc-switch'
import SingleSelect from './SingleSelect'
import SingleSelectAsync from './SingleSelectAsync'

const FormField = ({ type, name, fieldProps = {}, getFieldProps, value, error, touched, setFieldTouched, setFieldValue, ...props }) => {

    if (type == 'switch') {
        return (
            <Switch
                className="custom-switch custom-switch-primary"
                checked={value}
                onChange={(val) => setFieldValue(name, val ? 1 : 0)}
            />
        )
    }

    if (type == 'editor') {
        return (
            <Editor
                name={name}
                onBlur={setFieldTouched}
                onChange={setFieldValue}
                invalid={Boolean(error && touched)}
                value={value}
                {...fieldProps}
            />
        )
    }

    if (type == 'date') {
        return (
            <DatePicker
                name={name}
                onBlur={setFieldTouched}
                onChange={setFieldValue}
                selected={value}
                invalid={Boolean(error && touched)}
                {...fieldProps}
            />
        )
    }

    if (type == "single-select") {
        return (
            <SingleSelect
                name={name}
                onBlur={setFieldTouched}
                onChange={setFieldValue}
                value={value}
                invalid={Boolean(error && touched)}
                {...fieldProps}
            />
        )
    }

    if (type == "single-select-async") {
        return (
            <SingleSelectAsync
                name={name}
                onBlur={setFieldTouched}
                onChange={setFieldValue}
                value={value}
                invalid={Boolean(error && touched)}
                {...fieldProps}
            />
        )
    }

    if (type == "file") {
        return (
            <CustomInput
                id={'file-' + name}
                className={`${Boolean(error && touched) ? 'is-invalid' : ''}`}
                type={type}
                name={name}
                onChange={({ target }) => setFieldValue(name, target.files)}
                onBlur={() => setFieldTouched(name, true)}
                {...fieldProps}
            />
        )
    }

    return (
        <Input
            type={type}
            {...getFieldProps(name)}
            {...fieldProps}
            invalid={Boolean(error && touched)}
        />
    )

}

const FormGenerator = ({
    submitLabel="Submit",
    formName = "",
    fields = [],
    values,
    getFieldProps,
    handleSubmit,
    errors,
    touched,
    setFieldValue,
    setFieldTouched,
    setValues,
    isSubmitting,
    isValidating,
    ...props
}) => {

    useFocusError(errors, isSubmitting, isValidating)

    return (
        <Form name={formName} onSubmit={handleSubmit} autoComplete="off" autoCorrect="off">
            <Row>
                {
                    fields.map((item, inx) => {
                        let { size = 12, label, name, type = "text", Field = FormField, props: fieldProps = {}, Info = () => '', ...restProps } = item

                        let formFieldProps = {
                            type,
                            name,
                            value: values[name] || null,
                            error: errors[name] || null,
                            touched: touched[name] || null,
                            getFieldProps,
                            fieldProps,
                            setFieldTouched,
                            setFieldValue
                        }
                        return (
                            <Col xs={size} key={inx}>
                                <FormGroup>
                                    {label != '' && <Label>{label}</Label>}
                                    <Field {...formFieldProps} />
                                    <FormFeedback>{errors[name]}</FormFeedback>
                                    <Info {...formFieldProps} />
                                </FormGroup>
                            </Col>
                        )
                    })
                }
            </Row>
            <Button
                color="primary"
                className={`btn-shadow btn-multiple-state ${isSubmitting ? 'show-spinner' : ''}`}
                size="lg"
                type="submit"
                disabled={isSubmitting}
            >
                <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                </span>
                <span className="label">
                    {submitLabel}
                </span>
            </Button>
        </Form>
    )
}

export default FormGenerator