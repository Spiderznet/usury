import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';
import { ProtectedRoute, UserRole } from '../../helpers/authHelper';


const Dashboard = React.lazy(() =>
    import(/* webpackChunkName: "viwes-dashboard-page" */ './dashboard')
);

const Users = React.lazy(() =>
    import(/* webpackChunkName: "viwes-users-page" */ './users')
);

const Loans = React.lazy(() =>
    import(/* webpackChunkName: "viwes-loans-page" */ './loans')
);

const Transactions = React.lazy(() =>
    import(/* webpackChunkName: "viwes-transactions-page" */ './transactions/Transactions')
);

const Instalments = React.lazy(() =>
    import(/* webpackChunkName: "viwes-instalments-page" */ './instalments/Instalments')
);

const Expenses = React.lazy(() =>
    import(/* webpackChunkName: "viwes-expenses-page" */ './expenses/Expenses')
);


const App = ({ match }) => {
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <Suspense fallback={<div className="loading" />}>
                    <Switch>
                        <Redirect exact from={`${match.url}/`} to={`${match.url}/dashboard`} />
                        <ProtectedRoute
                            path={`${match.url}/dashboard`}
                            component={Dashboard}
                            roles={[UserRole.Admin, UserRole.root, UserRole.collector]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/users`}
                            component={Users}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/loans`}
                            component={Loans}
                            roles={[UserRole.Admin, UserRole.root, UserRole.collector]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/transactions`}
                            component={Transactions}
                            roles={[UserRole.Admin, UserRole.root, UserRole.collector]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/instalments`}
                            component={Instalments}
                            roles={[UserRole.Admin, UserRole.root, UserRole.collector]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/expenses`}
                            component={Expenses}
                            roles={[UserRole.Admin, UserRole.root, UserRole.collector]}
                        />

                        <Redirect to="/error" />
                    </Switch>
                </Suspense>
            </div>
        </AppLayout>
    );
};

const mapStateToProps = ({ menu }) => {
    const { containerClassnames } = menu;
    return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
