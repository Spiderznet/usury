import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle, Label, Input, Col, FormGroup, Button } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { insert, useFormik } from 'formik'
import * as yup from 'yup'
import { UserServices, LoanServices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import { adminRoot } from '../../../constants/defaultValues';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { toFormData } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import { UserRole } from '../../../helpers/authHelper';

const CreateLoan = ({ match, ...props }) => {
    let { id = "" } = match.params
    let [options, setOptions] = useState({})

    let [principalAmount, setPrincipalAmount] = useState('')
    let [interest, setInterest] = useState('')
    let [interestType, setInterestType] = useState('MONTH')
    let [duration, setDuration] = useState('')
    let [durationType, setDurationType] = useState('MONTH')
    let [instalmentCycle, setInstalmentCycle] = useState('')
    let [instalmentCycleType, setInstalmentCycleType] = useState('DAY')

    let name = "Loan";
    const [isLoading, setIsLoading] = useState(id ? true : false)

    const formSchema = yup.object().shape({

        user_id: yup.mixed()
            .test('required', "User is required", val => val && val.name),
        principal_amount: yup.number()
            .typeError('Enter valid Principal Amount')
            .required('Principal Amount is Requires'),
        intrest: yup.number()
            .typeError('Enter valid Intrest')
            .required('Intrest is Requires'),
        total_amount: yup.number()
            .typeError('Enter valid Total Amount')
            .required('Total Amount is Requires'),
        no_of_instalment: yup.number()
            .typeError('Enter valid No Of Instalment')
            .required('No Of Instalment is Requires'),
        instalment_amount: yup.number()
            .typeError('Enter valid Instalment Amount')
            .required('Instalment Amount is Requires'),
    })



    useEffect(() => {
        // LoanServices.addInfo().then(({ status, data }) => {
        //     if (status) {
        //         setOptions({
        //             ...options,
        //             roles: data.roles.map(item => ({ name: item.name, id: item.role_id }))
        //         })
        //     }
        // })
        if (id) {
            LoanServices.details(id).then(({ status, data }) => {
                if (status) {

                    setValues({
                        user_id: { id: data.user.id, name: `${data.user.id} - ${data.user.name}` },
                        pan_number: data.pan_number,
                        gst_number: data.gst_number,
                        company_type: { name: data.companyType.name, id: data.companyType.company_type_id },
                        address_type: { name: data.addressType.name, id: data.addressType.address_type_id },
                        address_number: data.address_number,
                        is_completed: data.is_completed,
                    })
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            user_id: '',
            principal_amount: '',
            intrest: '',
            intrest_amount: '',
            interest_type: '',
            total_amount: '',
            duration: '',
            duration_type: '',
            instalment_cycle: '',
            instalment_cycle_type: '',
            no_of_instalment: '',
            instalment_amount: '',
        },
        validationSchema: formSchema,
        onSubmit: ({ ...values }, { setSubmitting, resetForm, setFieldError }) => {

            if (id) {
                LoanServices.update(id, values).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/loans/view/${data.id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                LoanServices.insert(values).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/loans/view/${data.id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                _where: {
                    role_id_eq: UserRole.loaner
                },
                search
            }, { encode: false })
        }
        UserServices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }
    let rateOfInterest = Number(parseFloat((interest/principalAmount)*100).toFixed(2))

    let fields = [
        { name: 'user_id', label: 'User', type: "single-select-async", props: { loadOptions: loadUsers } },
        { name: 'principal_amount', label: 'Principal Amount', props: { disabled: true } },
        { name: 'intrest', label: `Intrest (${rateOfInterest ? rateOfInterest + '%' : ''})`, props: { disabled: true } },
        { name: 'total_amount', label: 'Total Amount', props: { disabled: true } },
        { name: 'no_of_instalment', label: `No Of Instalment (${instalmentCycle ? 'every ' + instalmentCycle + " " + instalmentCycleType.toLocaleLowerCase() : ""})`, props: { disabled: true } },
        { name: 'instalment_amount', label: 'Instalment Amount', props: { disabled: true } },
    ]

    const calculate = () => {
        let typeVal = {
            YEAR: 1,
            MONTH: 12,
            DAY: 365,
        }
        

        let totalAmount = Number(principalAmount) + Number(interest);
        let instalmentCycleTypeVal = {
            YEAR: 365,
            MONTH: 30,
            DAY: 1,
        }
        let noOfInstalment = Math.round((duration * instalmentCycleTypeVal[durationType]) / (instalmentCycle * instalmentCycleTypeVal[instalmentCycleType]))
        let instalmentAmount = (Math.round((totalAmount / noOfInstalment)))

        console.log(`
        Principal Amount: ${principalAmount}
        intrest Amount: ${interest}
        Total Amount: ${totalAmount}
        No Of Instalment: ${noOfInstalment}
        Instalment Amount: ${instalmentAmount}
        `)
        setValues({
            principal_amount: principalAmount,
            intrest: rateOfInterest,
            intrest_amount: interest,
            interest_type: interestType,
            total_amount: totalAmount,
            duration,
            duration_type: durationType,
            instalment_cycle: instalmentCycle,
            instalment_cycle_type: instalmentCycleType,
            no_of_instalment: noOfInstalment,
            instalment_amount: instalmentAmount,
        })
    }

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                {name} Detail Form
                            </CardTitle>
                            <Row className="form-group">
                                <Label sm={4}>Principal Amount</Label>
                                <Col sm><Input type="number" value={principalAmount} onChange={({ target }) => setPrincipalAmount(target.value)} /></Col>
                            </Row>
                            <Row className="form-group">
                                <Label sm={4}>Interest</Label>
                                <Col><Input type="number" value={interest} onChange={({ target }) => setInterest(target.value)} /></Col>
                                {/* <Col>
                                    <Input type="select" value={interestType} onChange={({ target }) => setInterestType(target.value)}>
                                        <option value="YEAR">Year</option>
                                        <option value="MONTH">Month</option>
                                        <option value="DAY">Day</option>
                                    </Input>
                                </Col> */}
                            </Row>
                            <Row className="form-group">
                                <Label sm={4}>Duration</Label>
                                <Col><Input type="number" value={duration} onChange={({ target }) => setDuration(target.value)} /></Col>
                                <Col>
                                    <Input type="select" value={durationType} onChange={({ target }) => setDurationType(target.value)}>
                                        <option value="YEAR">Year</option>
                                        <option value="MONTH">Month</option>
                                        <option value="DAY">Day</option>
                                    </Input>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label sm={4}>Instalment Cycle</Label>
                                <Col><Input type="number" value={instalmentCycle} onChange={({ target }) => setInstalmentCycle(target.value)} /></Col>
                                <Col>
                                    <Input type="select" value={instalmentCycleType} onChange={({ target }) => setInstalmentCycleType(target.value)}>
                                        <option value="YEAR">Year</option>
                                        <option value="MONTH">Month</option>
                                        <option value="DAY">Day</option>
                                    </Input>
                                </Col>
                            </Row>
                            <FormGroup>
                                <Button
                                    color="primary"
                                    onClick={calculate}
                                    disabled={!principalAmount || !interest || !duration || !instalmentCycle}
                                >Calculate</Button>
                            </FormGroup>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />

                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateLoan