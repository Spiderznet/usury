import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row, Modal, ModalHeader, ModalBody, ButtonGroup } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { ExpensesServices, UserServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { formatAmount } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import * as yup from 'yup'
import { useFormik } from 'formik';
import { stringify } from 'qs';
import moment from 'moment'
import { getCurrentUser } from '../../../helpers/Utils';
import { UserRole } from '../../../helpers/authHelper';

const Expenses = ({ history, match, ...props }) => {

    let [list, setList] = useState([])
    let [isLoading, setIsloading] = useState(false)
    let [isprocess, setIsprocess] = useState(false)
    let [isOpen, setIsOpen] = useState(false)
    let [preview, setPreview] = useState(null)
    let [type, setType] = useState('All')
    let [duration, setDuration] = useState('today')
    let userDetails = getCurrentUser()

    let name = "Expenses";

    const getList = () => {
        setIsloading(true)
        let query = {_where:{}}
        if (userDetails.role != UserRole.root) {
            query['_where']['user_id_eq'] = userDetails.user_id;
        }
        if(type !== 'All') {
            query['_where']['description_eq'] = type
        }
        if(duration !== 'all') {
            if(duration === 'today') {
                query['_where']['created_at_gt'] = moment().startOf('day').toDate()
            }
            if(duration === 'yesterday') {
                query['_where']['created_at_gt'] = moment().subtract(1,'day').startOf('day').toDate()
                query['_where']['created_at_lt'] = moment().subtract(1,'day').endOf('day').toDate()
            }
            if(duration === 'last7Days') {
                query['_where']['created_at_gt'] = moment().subtract(7,'day').startOf('day').toDate()
            }
            if(duration === 'last30Days') {
                query['_where']['created_at_gt'] = moment().subtract(30,'day').startOf('day').toDate()
            }
            if(duration === 'thisMonth') {
                query['_where']['created_at_gt'] = moment().startOf('month').toDate()
            }
        }
        const queryStr = "?" + stringify(query, { encode: false })
        ExpensesServices.list(queryStr).then(({ status, data }) => {
            if (status) {
                setList(data)
                setIsloading(false)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [duration,type])

    const deleteRow = (value) => {
        // console.log(value.id);
        ExpensesServices.delete(value.id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'User',
            accessor: 'user',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value?.name} ({props.value?.id})</>,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <div className={`${props.value > 0 ? 'text-success' : 'text-danger'}`}>{formatAmount(props.value)}</div>,
        },
        {
            Header: 'Description',
            accessor: 'description',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Created At',
            accessor: 'created_at',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{moment(props.value).format('MMMM Do YYYY, h:mm:ss a')}</>,
        },
    ], []);

    const formSchema = yup.object().shape({
        amount: yup.number()
            .typeError('Enter valid amount')
            .required('Amount is Requires'),
        description: yup.mixed()
            .test('required', "Description is required", val => val && val.name),
        otherDescription: yup.mixed()
            .test('required', "This field is required", function (val) {
                let { description } = this.parent
                if (description && description.name === "Other") {
                    return val != ''
                }
                return true
            }),

    })


    const formProps = useFormik({
        initialValues: {
            amount: '',
            description: '',
        },
        validationSchema: formSchema,
        onSubmit: ({ id = "", ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let params = {
                amount: values.amount
            }
            params['description'] = values.description.name
            if (values.description.name === 'Other') {
                params['description'] = values.otherDescription
            }
            if (id) {
                ExpensesServices.update(id, params).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            } else {
                ExpensesServices.insert(params).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating, resetForm } = formProps

    const toggle = () => {
        setIsOpen(!isOpen)
        resetForm()
        setPreview(null)
    }

    const getDetails = (id) => {
        setIsprocess(true)
        toggle();
        ExpensesServices.details(id).then(({ status, data }) => {
            if (status) {
                setValues({
                    amount: data.amount,
                    description: data.description,
                })
                setPreview(data.slide)
                setIsprocess(false)
            }
        })
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    let options = [
        { id: 'Tea', name: 'Tea' },
        { id: 'Coffee', name: 'Coffee' },
        { id: 'Petrol', name: 'Petrol' },
        { id: 'Food', name: 'Food' },
        { id: 'Other', name: 'Other' },
    ]
    let fields = [
        { name: 'amount', label: 'Amount' },
        { name: 'description', label: 'Description', type: "single-select", props: { options } },
    ]

    if (values.description && values.description.name === "Other") {
        fields = [
            ...fields,
            { name: 'otherDescription', label: 'Description', type: "textarea" },
        ]
    }

    if (isLoading) {
        return <div className="loading" />
    }

    const tableHeader = (toggleFilter) => {
        return (
            <div className='d-flex'>
                <div>
                    <ButtonGroup className="mr-3">
                        {['All','Tea','Coffee','Petrol','Food'].map((item,inx) => {
                            return (
                                <Button color={type===item?'primary':'light'} onClick={() => setType(item)} key={inx}>{item}</Button>
                            )
                        })}
                    </ButtonGroup>
                    <ButtonGroup>
                        <Button color={duration === 'all' ? 'primary' : 'light'} onClick={() => setDuration('all')} >All</Button>
                        <Button color={duration === 'today' ? 'primary' : 'light'} onClick={() => setDuration('today')} >Today</Button>
                        <Button color={duration === 'yesterday' ? 'primary' : 'light'} onClick={() => setDuration('yesterday')} >Yesterday</Button>
                        <Button color={duration === 'last7Days' ? 'primary' : 'light'} onClick={() => setDuration('last7Days')} >Last 7 Days</Button>
                        <Button color={duration === 'last30Days' ? 'primary' : 'light'} onClick={() => setDuration('last30Days')} >Last 30 Days</Button>
                        <Button color={duration === 'thisMonth' ? 'primary' : 'light'} onClick={() => setDuration('thisMonth')} >This Month</Button>
                    </ButtonGroup>
                </div>
                <div className='ml-auto'>
                    <Button color="primary" onClick={toggleFilter} ><i className="iconsminds-filter-2" />Filter</Button>
                </div>
            </div>
        )
    }

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Button color="primary" size="lg" onClick={toggle}>Add New</Button>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>
                <Colxx xxs="12">

                    
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable className="table-responsive" columns={cols} data={list} divided header={tableHeader} />
                </Colxx>
            </Row>
            <Modal wrapClassName="modal-right" isOpen={isOpen} toggle={toggle}>
                <ModalHeader toggle={toggle}>{values.id ? 'Update Details' : 'Add New'}</ModalHeader>
                <ModalBody>
                    {
                        isprocess ?
                            (
                                <div className="loading" />
                            ) : (
                                <FormGenerator
                                    fields={fields}
                                    {...formProps}
                                />
                            )
                    }
                </ModalBody>
            </Modal>
        </React.Fragment>
    )
}

export default Expenses