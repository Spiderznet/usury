import Axios from 'axios';
import { NotificationManager } from '../components/common/react-notifications';
import { API_ROOT } from '../config';
import data from '../data/notifications';

const getOptions = () => {
    let options = {};
    let token = localStorage.getItem('token');
    if (token) {
        options = {
            ...options,
            headers: {
                'x-token': token
            }

        }
    }

    return options
}

const parseResponse = res => res.data;

const errorInfo = (res, err) => {
    if (!res.status) {
        NotificationManager.error(res.data, res.msg, 3000, null, null, '');
        // throw new Error(res.msg)
    }
    if (err) {
        NotificationManager.error(err.message, err.name, 3000, null, null, '');
        // throw err
    }
    return res
}

const http = {
    get: url => Axios.get(`${API_ROOT}${url}`, getOptions()).then(parseResponse).then(errorInfo),
    post: (url, data) => Axios.post(`${API_ROOT}${url}`, data, getOptions()).then(parseResponse).then(errorInfo),
    put: (url, data) => Axios.put(`${API_ROOT}${url}`, data, getOptions()).then(parseResponse).then(errorInfo),
    delete: url => Axios.delete(`${API_ROOT}${url}`, getOptions()).then(parseResponse).then(errorInfo),
}

const AuthServices = {
    check: data => http.post('/auth/check', data),
    login: (username, password) => http.post('/auth/login', { username, password }),
    forgotPassword: data => http.post('/auth/forgot', data),
    verifyForgotPasswordOTP: data => http.post('/auth/forgot-verify-otp', data),
    resetPassword: data => http.post('/auth/forgot-reset', data),
    resendForgotPasswordOTP: data => http.post('/auth/forgot-resend-otp', data),
    changeChurch: data => http.post('/auth/change-church', data)
}

const UserServices = {
    list: (params = "") => http.get(`/admin/users${params}`),
    addInfo: () => http.get(`/admin/users/add-info`),
    insert: (data) => http.post(`/admin/users`, data),
    details: (id) => http.get(`/admin/users/${id}`),
    update: (id, data) => http.put(`/admin/users/${id}`, data),
    delete: (id) => http.delete(`/admin/users/${id}`)
}

const PaymentsServices = {
    list: (params = "") => http.get(`/admin/payments${params}`),
    addInfo: () => http.get(`/admin/payments/add-info`),
    insert: (data) => http.post(`/admin/payments`, data),
    details: (id) => http.get(`/admin/payments/${id}`),
    update: (id, data) => http.put(`/admin/payments/${id}`, data),
    delete: (id) => http.delete(`/admin/payments/${id}`)
}

const InstalmentsServices = {
    list: (params = "") => http.get(`/admin/instalments${params}`),
    addInfo: () => http.get(`/admin/instalments/add-info`),
    insert: (data) => http.post(`/admin/instalments`, data),
    details: (id) => http.get(`/admin/instalments/${id}`),
    update: (id, data) => http.put(`/admin/instalments/${id}`, data),
    delete: (id) => http.delete(`/admin/instalments/${id}`)
}

const ExpensesServices = {
    list: (params = "") => http.get(`/admin/expenses${params}`),
    addInfo: () => http.get(`/admin/expenses/add-info`),
    insert: (data) => http.post(`/admin/expenses`, data),
    details: (id) => http.get(`/admin/expenses/${id}`),
    update: (id, data) => http.put(`/admin/expenses/${id}`, data),
    delete: (id) => http.delete(`/admin/expenses/${id}`)
}

const LoanServices = {
    list: (params = "") => http.get(`/admin/loans${params}`),
    addInfo: () => http.get(`/admin/loans/add-info`),
    insert: (data) => http.post(`/admin/loans`, data),
    details: (id) => http.get(`/admin/loans/${id}`),
    update: (id, data) => http.put(`/admin/loans/${id}`, data),
    delete: (id) => http.delete(`/admin/loans/${id}`)
}

const LocationServices = {
    getCountry: (params = "") => http.get(`/locations/country${params}`),
    getState: (params = "") => http.get(`/locations/state${params}`),
    getCity: (params = "") => http.get(`/locations/city${params}`)
}
const DashboardServices = {
    getDashboardDetail: () => http.get('/admin/dashboard')
}



export {
    http,
    AuthServices,
    UserServices,
    LocationServices,
    DashboardServices,
    PaymentsServices,
    LoanServices,
    InstalmentsServices,
    ExpensesServices
}